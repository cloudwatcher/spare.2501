# -*- coding: utf-8 -*-
import os
import web

SINA_APP_KEY = "3807638853"
SINA_APP_SECRET = "55ce17c8f7a077767e3f8280a4a9d7ef"
SINA_CALLBACK_URL = 'http://2501.sinaapp.com/oauth/weibo'

DOUBAN_API_KEY = "d11feaa732f78064"
DOUBAN_API_SECRET = "015434d2d144350803fc9e7873cc9f18"
DOUBAN_CALLBACK_URL = 'http://2501.sinaapp.com/oauth/douban'

QQ_API_KEY = "100240774"
QQ_API_SECRET = "7c8f96ab900aa9c8f9bbc081494e9a12"
QQ_CALLBACK_URL = "http://2501.sinaapp.com/oauth/tc"
QQ_API_DOMAIN = "graph.qq.com"
QQ_API_SUFFIX = "oauth2.0"

RENREN_API_KEY = "8248830fd2e542669bfbb12c127eeaf1"
RENREN_API_SECRET = "d72078dc7af4480688bb6a3c7fd7974a"
RENREN_CALLBACK_URL = "http://2501.sinaapp.com/oauth/rr"
RENREN_API_DOMAIN = "graph.renren.com"
RENREN_API_SUFFIX = "oauth"

TSET = {}
TSET['User'] = 'users'

from pylog.auth import Cookie
from modules.Users import Users
User = Users(TSET['User'])
cookie = Cookie(User)

class oauth:
	"""docstring for oauth"""
	def GET(self,kind):
		if kind == "sina":
			from pylog.weibo import APIClient
			client = APIClient(app_key=SINA_APP_KEY, app_secret=SINA_APP_SECRET, redirect_uri=SINA_CALLBACK_URL)
			url = client.get_authorize_url()
			return web.seeother(url)
		elif kind == "qq": #https://graph.qq.com/oauth2.0/authorize
			from pylog.weibo import APIClient
			client = APIClient(app_key=QQ_API_KEY, app_secret=QQ_API_SECRET, redirect_uri=QQ_CALLBACK_URL,domain=QQ_API_DOMAIN,suffix=QQ_API_SUFFIX)
			url = client.get_authorize_url()
			return web.seeother(url)
		elif kind == "renren":
			from pylog.weibo import APIClient
			client = APIClient(app_key=RENREN_API_KEY, app_secret=RENREN_API_SECRET, redirect_uri=RENREN_CALLBACK_URL,domain=RENREN_API_DOMAIN,suffix=RENREN_API_SUFFIX)
			url = client.get_authorize_url()
			return web.seeother(url)
		else:
			if kind == "weibo":
				from pylog.weibo import APIClient
				import time
				i = web.input()
				code = i.code
				client = APIClient(app_key=SINA_APP_KEY, app_secret=SINA_APP_SECRET, redirect_uri=SINA_CALLBACK_URL)
				r = client.request_access_token(code)
				access_token = r.access_token
				expires_in = r.expires_in
				client.set_access_token(access_token, expires_in)
				#Connection Complete
				uid = r.uid
				user = client.get.users__show(uid=uid)
				if len(User._get(sina_id=int(uid))) != 0:
					User.sdb.update(User.table,where='sina_id=$uid', vars=dict(uid=uid),title=user.screen_name,gender=user.gender,province=user.province,city=user.city,avatar=user.profile_image_url,sina_access_tolken = access_token)
				else:
					User.add(type="guest",level=100,sina_id=user.id,title=user.screen_name,gender=user.gender,province=user.province,city=user.city,avatar=user.profile_image_url,sina_access_tolken = access_token)
				#Create Cookie
				uis = ",".join([str(uid),access_token])
				cookie.SET(COOKIE_EXPIRE=int(expires_in-time.time()),uis=uis)
				return web.seeother('/')
			elif kind == "tc":
				from pylog.weibo import APIClient
				client = APIClient(app_key=QQ_API_KEY, app_secret=QQ_API_SECRET, redirect_uri=QQ_CALLBACK_URL,domain=QQ_API_DOMAIN,suffix=QQ_API_SUFFIX)
				import time
				i = web.input()
				code = i.code
				openid,access_token,expires_in = client.request_access_token(code,suffix="token",state="1",site="qq")
				client.api_url = "http://" + QQ_API_DOMAIN + '/'
				client.set_access_token(access_token,expires_in+int(time.time()))
				url = "https://" + QQ_API_DOMAIN + '/user/get_user_info?'
				user = client._get(url,openid=openid,access_token=access_token,oauth_consumer_key=QQ_API_KEY)
				#{'figureurl_1': u'http://qzapp.qlogo.cn/qzapp/100240774/1BB4AFBD97399F41E877E31EDA35A50D/50', 'figureurl': u'http://qzapp.qlogo.cn/qzapp/100240774/1BB4AFBD97399F41E877E31EDA35A50D/30', 'figureurl_2': u'http://qzapp.qlogo.cn/qzapp/100240774/1BB4AFBD97399F41E877E31EDA35A50D/100', 'level': u'0', 'gender': u'\u7537', 'ret': 0, 'vip': u'0', 'msg': u'', 'nickname': u'Yao\xa0Yuan'}
				if len(User._get(qq_id=openid)) != 0:
					User.sdb.update(User.table,where='qq_id=$uid', vars=dict(uid=openid),title=user.nickname,gender=user.gender,avatar=user.figureurl,qq_access_tolken = access_token)
				else:
					User.add(type="guest",level=100,qq_id=openid,title=user.nickname,gender=user.gender,avatar=user.figureurl,qq_access_tolken = access_token)
				uis = ",".join([str(openid),access_token])
				cookie.SET(COOKIE_EXPIRE=int(expires_in),uis=uis)
				return web.seeother('/')
			elif kind == "rr":
				from pylog.weibo import APIClient
				client = APIClient(app_key=RENREN_API_KEY, app_secret=RENREN_API_SECRET, redirect_uri=RENREN_CALLBACK_URL,domain=RENREN_API_DOMAIN,suffix=RENREN_API_SUFFIX)
				import time
				i = web.input()
				code = i.code
				r = client.request_access_token(code,suffix="token",state="1",site="renren")
				user = r.user
				access_token = r.access_token
				expires_in = r.expires_in
				if len(User._get(rr_id=user.id)) != 0:
					User.sdb.update(User.table,where='rr_id=$uid', vars=dict(uid=user.id),title=user.name,avatar=user.avatar[0].url,rr_access_tolken = access_token)
				else:
					User.add(type="guest",level=100,rr_id=user.id,title=user.name,avatar=user.avatar[0].url,rr_access_tolken = access_token)
				uis = ",".join([str(user.id),access_token])
				cookie.SET(COOKIE_EXPIRE=int(expires_in),uis=uis)
				return web.seeother('/')
			return "ILLEGAL ACCESS"