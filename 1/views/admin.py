# -*- coding: utf-8 -*-
import os
import web

from pylog.common import striptag
from pylog.common import add_class

from pylog.parts import CommonPartsFront
from pylog.adminparts import Common
from pylog.forms import Login

TSET = {}
TSET['User'] = 'users'
TSET['Link'] = 'links'
TSET['Blog'] = 'blogs'
TSET['Note'] = 'notes'
TSET['Comment'] = 'comments'
TSET['Setting'] = 'settings'
TSET['Tags'] = 'tags'

from pylog.auth import Cookie
from modules.Users import Users
User = Users(TSET['User'])
cookie = Cookie(User)


TEMPLATE_PATH = '../templates/themes/desktop/'
THEME = 'administration'

SNIPPETS = {}
SNIPPETS['strip'] = striptag
SNIPPETS['addClass'] = add_class

CPF = CommonPartsFront()
C = Common()

NODE = [TSET['Blog'],TSET['Note'],TSET['Comment'],TSET['User'],'node']


app_root = os.path.abspath(os.path.join('..',os.path.dirname(__file__)))

templates_root = os.path.join(app_root,TEMPLATE_PATH+THEME)

render = web.template.render(templates_root,globals=SNIPPETS)

renderDict = {}

class login:
	def GET(self):
		user = cookie.GET()
		if user:
			return web.seeother("/manage/")
		else:
			from modules.Settings import Settings
			title = Settings(TSET['Setting'])._get(title="title")[0].value
			renderDict = {'login':Login(),'name':'Stranger','title':title}
			return render.login(renderDict)

	def POST(self):
		login = Login()
		if not login.validates():
			return "Did not validate"
		else:
			#go and look for something in the database
			#change the cookie
			username = login.username.value
			password = login.password.value
			result = User.BasicAuth(username,password)
			if result:
				uis = ",".join([result.mail_addr,result.password])
				cookie.SET(uis=uis)
				#render other page
				return web.seeother('/Admin/Login/')
			else:
				return "Login Failed"

class manage:
	def GET(self,title):
		user = cookie.GET()
		if not user:return web.seeother("/Admin/Login/")
		from modules.Links import Links
		Links = Links(TSET['Link'])
		if title == "":
			link = Links._get(url="/Admin/Login/",type="admin_nav")[0]
		else:
			request = title.split('/')
			kind = request[0]
			page = request[1]
			if page == "":page = 1
			myvar = dict(title=kind,type="admin_nav",weight=user.level)
			try:
				link = Links.sdb.select(Links.table,myvar,where="title=$title AND type=$type AND weight < $weight")[0]
			except IndexError:
				return "ILLEGAL ACCESS"
		if user:
			renderDict['head'] = C.head()
			renderDict['title'] = C.title(link.title)
			renderDict['header'] = C.header(link.url)
			renderDict['footer'] = C.footer()
			if link.url == "/Admin/Login/":
				self._dashboard()
			else:
				if link.url in NODE:
					self._node_list(link.url,page,link)
				if link.url == TSET['Setting']:
					self.site_settings()
				if link.url == TSET['Link']:
					self.navi_settings()
				if link.url == "_myaccount":
					self.myaccount()
				if link.url.split('__')[0] == "add":
					objectType = link.url.split('__')[1]
					self._add_node(objectType,link)
				if link.url.split('__')[0] == "edit":
					objectType = link.url.split('__')[1]
					self._editNode(objectType,link,page)
			return render.base(renderDict)
		else:
			return web.seeother("/Admin/Login/")

	def POST(self,para):
		user = cookie.GET()
		if not user:return web.seeother("/Admin/Login/")
		i = web.input()
		from modules.Links import Links
		Links = Links(TSET['Link'])
		title = para.split('/')[0]
		ObjectId = para.split('/')[1]
		myvar = dict(title=title,type='admin_nav',weight=user.level)
		try:
			link = Links.sdb.select(Links.table,myvar,where="title=$title AND type=$type AND weight < $weight")[0]
		except IndexError:
			return "ILLEGAL ACCESS"
		request = link.url.split('__')
		action = request[0]
		ObjectType = request[1]
		if ObjectType in NODE:
			from modules.Nodes import Nodes
			Node = Nodes(ObjectType)
			title = str(i.producttitle.encode("utf-8","ignore"))
			body = str(i.nodedesc.encode("utf-8","ignore"))
			author = user.id
			if title == "" or body == "":
				return "FILL IN ALL NECESSARY BOXES"
			if action == 'add':
				Node.add(title,body,author)
			elif action == 'edit':
				Node.update(title,body,author,int(ObjectId))
		return action,ObjectType,i,user
	def _dashboard(self):
		user = cookie.GET()
		renderDict['left'] = C.dashboard(user)
		renderDict['right']= C.myinfo(user)
	
	def _node_list(self,node,page,link):
		from modules.Nodes import Nodes
		rdate = 1
		author = TSET['User']
		if node in [TSET['Note'],TSET['User']]:rdate = 0
		if node == TSET['User']:author = False
		nodes = Nodes(node).page(int(page),8,AUTHOR=author,DATE=rdate,BODY=False,ID=True)
		data = []
		for n in nodes:
			data.append([n.values()])
		left = C._table(data,node,tk="display stylized")
		renderDict['left'] = left
		right = C.right_menu(node,link)
		renderDict['right'] = right
	
	def site_settings(self):
		from modules.Nodes import Nodes
		from modules.Settings import Settings
		settings = Settings(TSET['Setting'])._get(type="all")
		save = Nodes(TSET['Note'])._get(title="save",type="sysctr")[0].body
		content = Nodes(TSET['Note'])._get(type="sys_set_ct")[0]
		renderDict['left'] = C.settings(settings)
		btn = C._button_spec("green",save,"#")
		ctn = C._content_box(content.title,content.body)
		renderDict['right'] = C.TF.html([
			C.TF.p(btn),
			C.TF.p(ctn)
			])
	
	def navi_settings(self):
		from modules.Nodes import Nodes
		from modules.Links import Links
		out_links = Links(TSET['Link'])._get(type="navigation")
		admin_links = Links(TSET['Link'])._get(type="admin_nav")
		content = Nodes(TSET['Note'])._get(type="sys_set_ct")[0]
		renderDict['left'] = C.navigation_settings(out_links,admin_links)
		ctn = C._content_box(content.title,content.body)
		renderDict['right'] = C.TF.html(C.TF.p(ctn))
	
	def myaccount(self):
		from modules.Nodes import Nodes
		content = Nodes(TSET['Note'])._get(type="sys_mypage")[0]
		ctn = C._content_box(content.title,content.body)
		user = cookie.GET()
		renderDict['left'] = C.myaccount(user)
		renderDict['right'] = C.myinfo(user)
	
	def _add_node(self,NodeType,link):
		if not NodeType in NODE:
			renderDict['left'] = 'Invalid Type'
			renderDict['right'] = 'Warning'
		else:
			renderDict['left'] = C.node(NodeType=NodeType,link=link,node=None)
			renderDict['right'] = C.node_side(NodeType=NodeType,tags=None)
	
	def _editNode(self,objectType,link,id):
		if not objectType in NODE:
			renderDict['left'] = 'Invalid Type'
			renderDict['right'] = 'Warning'
		else:
			from modules.Nodes import Nodes
			from modules.Tags import Tags
			node = Nodes(objectType)._get(id=id)[0]
			tags = Tags(TSET['Tags'])._get(rid=id,type=objectType)
			renderDict['left'] = C.node(NodeType=objectType,link=node,node=node)
			renderDict['right'] = C.node_side(NodeType=objectType,tags=tags)

class edit:
	"""docstring for edit"""
	def GET():
		pass