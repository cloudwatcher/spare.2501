# -*- coding: utf-8 -*-
import os
import web

from pylog.common import striptag
from pylog.parts import CommonPartsFront

TEMPLATE_PATH = '../templates/themes/desktop/'
THEME = 'emplode'

SNIPPETS = {}
SNIPPETS['strip'] = striptag

CPF = CommonPartsFront()

LOCAL_DEBUG = 1

if not LOCAL_DEBUG:
	app_root = os.path.abspath(os.path.join('..',os.path.dirname(__file__)))
else:
	app_root = os.path.abspath(os.path.dirname(__file__))

templates_root = os.path.join(app_root,TEMPLATE_PATH+THEME)

render = web.template.render(templates_root,globals=SNIPPETS)

class index:
	def GET(self):
		renderDict = {}
		renderDict['head'] = CPF.head()
		renderDict['header'] = CPF.header()
		renderDict['footer'] = CPF.footer()
		renderDict['navigation'] = CPF.navigation('/')
		renderDict['main_content'] = CPF.node_list('Blog',1,Tag=1,Comment=1,User=1,render='nodelist')
		renderDict['boxes'] = [CPF.note('about'),CPF.link_box('blog_tag','side',tag=True),CPF.note('douban_alb')]
		renderDict['bottom_boxes'] = [CPF.link_box('friends','bottom'),CPF.link_box('sponsors','bottom'),CPF.link_box('technology','bottom')]
		return render.base(renderDict)