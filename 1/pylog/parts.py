# -*- coding: utf-8 -*-
import os
import web
from pylog.common import split
from pylog.common import strfmonth

TEMPLATE_PATH = '../templates/themes/desktop/'
PARTS = '/parts'

TSET = {}
TSET['Setting'] = 'settings'
TSET['Link'] = 'links'
TSET['Blog'] = 'blogs'
TSET['Note'] = 'notes'
TSET['User'] = 'users'
TSET['Tag'] = 'tags'
TSET['Comment'] = 'comments'

Key_TITLE = 'title'
Key_TYPE = 'type'
SNIPPETS = {'split':split,'len':len,'strfmonth':strfmonth}

from pylog.auth import Cookie
from modules.Users import Users
User = Users(TSET['User'])
cookie = Cookie(User)


app_root = os.path.abspath(os.path.join('..',os.path.dirname(__file__)))
from modules.Settings import Settings
Settings = Settings(TSET['Setting'])

THEME = Settings.get(Key_TITLE,'theme')[0].value
templates_root = os.path.join(app_root,TEMPLATE_PATH+THEME+PARTS)
render = web.template.render(templates_root,globals=SNIPPETS)

PAGE_LENGTH = int(Settings.get(Key_TITLE,'PageLength')[0].value)

renderDict = {}
renderKeys = []

class CommonPartsFront():
	"""docstring for CommonParts"""
	def head(self):
		renderKeys = ['description','keywords','author','title']
		for key in renderKeys:
			renderDict[key] = Settings.get(Key_TITLE,key)[0].value
		renderDict['css'] = Settings.get(Key_TITLE,'css')
		return render.head(renderDict)
	
	def footer(self):
		renderDict['slogan'] = Settings.get(Key_TITLE,'slogan')[0].value
		from datetime import date
		renderDict['year'] = date.today().year
		return render.footer(renderDict)
	
	def header(self):
		renderKeys = ['slogan','title']
		for key in renderKeys:
			renderDict[key] = Settings.get(Key_TITLE,key)[0].value
		from modules.Links import Links
		Links = Links(TSET['Link'])
		renderDict['toplinks'] = Links.get(Key_TYPE,'top')
		user = cookie.GET()
		renderDict['user'] = user
		return render.header(renderDict)
	
	def navigation(self,current_link):
		from modules.Links import Links
		Links = Links(TSET['Link'])
		renderDict['links'] = Links.get(Key_TYPE,'navigation')
		renderDict['current_link'] = current_link
		return render.navigation(renderDict)
	
	def node_list(self,NodeType,PageNumber,**kwargs):
		WHERE = ""
		if PageNumber == '':
			PageNumber = 1
		else:
			PageNumber = int(PageNumber)

		from modules.Nodes import Nodes
		
		from pylog.common import getPageNum
		
		Nodes = Nodes(TSET[NodeType])
		renderDict['NodeType'] = NodeType
		renderDict['Page'] = getPageNum(PageNumber,Nodes.count('id'),PAGE_LENGTH)
		renderDict['CurrPage'] = PageNumber
		renderKeys = ['Tag','Comment','User']
		
		for key in renderKeys:
			if not kwargs[key]:
				TSET[key] = renderDict[key] = kwargs[key]
			else:
				renderDict[key] = kwargs[key]
		
		if 'year' in kwargs.keys():
			renderDict['kind'] = kwargs['year']
			from pylog.common import boundary
			B = boundary(int(kwargs['year']))
			WHERE = "WHERE node.rdate > '%s' AND node.rdate < '%s'" % (B[0],B[1])

		renderDict['nodes'] = Nodes.page(PageNumber,PAGE_LENGTH,AUTHOR=TSET['User'],TAG=TSET['Tag'],COMMENTCOUNT=TSET['Comment'],WHERE=WHERE)
		
		if kwargs['render'] == 'nodelist':
			return render.node_list(renderDict)
		elif kwargs['render'] == 'archive':
			return render.archive(renderDict)
	
	def node(self,NodeName,NodeType):
		from modules.Nodes import Nodes
		Node = Nodes(TSET[NodeType])
		Comments = Nodes(TSET['Comment'])
		from modules.Users import Users
		User = Users(TSET['User'])
		from modules.Tags import Tags
		Tags = Tags(TSET['Tag'])
		renderDict['node'] = Node.get('title',NodeName)[0]
		renderDict['author'] = User.get('id',renderDict['node'].author)[0].title
		renderDict['comments'] = Comments.parent(TSET['User'],renderDict['node'].id)
		renderDict['tags'] = Tags.sdb.select(
			Tags.table,
			where=web.db.sqlwhere({'rid':renderDict['node'].id,'type':TSET[NodeType]})
			)
		return render.node(renderDict)
	
	def note(self,NoteName):
		from modules.Nodes import Nodes
		Note = Nodes(TSET['Note'])
		renderDict['note'] = Note.get(Key_TYPE,NoteName)[0]
		return render.about(renderDict)
	
	def links(self,pid):
		from modules.Links import Links
		Links = Links(TSET['Link'])
		renderDict['links'] = Links._get(parent=pid)
		return render.links(renderDict)
	
	def link_box(self,LinkType,pos,tag=False):
		from modules.Links import Links
		Links = Links(TSET['Link'])
		Parent = Links._get(type=LinkType,parent=0)[0]
		renderDict['linktitle'] = Parent.title
		renderDict['links'] = self.links(Parent.id)
		if pos == "side":
			if not tag:
				return render.category(renderDict)
			else:
				renderDict['links'] = Links._get(parent=Parent.id)
				return render.tags(renderDict)
		elif pos == "bottom":
			return render.bottom_box(renderDict)
	
	def images(self,ImageType):
		from modules.Images import Images
		Images = Image()
		renderDict['links'] = Images.get(ImageType)
		return render.images(renderDict)