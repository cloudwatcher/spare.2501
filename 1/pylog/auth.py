# -*- coding: utf-8 -*-
import web
from web import form
from pylog.common import encrypt
from pylog.common import decrypt

COOKIE_EXPIRE = 3600
COOKIE_DOMAIN = '2501.sinaapp.com'

class Cookie:
	"""
	docstring for CookieSet
	read, write the cookie
	"""
	def __init__(self,user):
		self.u = user

	def GET(self):
		"""set cookie"""
		try:
			"getting cookie"
			uis = decrypt(web.cookies().uis).split(',')
			username = uis[0]
			password = uis[1]
			BAuth = self.BasicAuth(username,password)
			SinaAuth = self.Sina_Auth(username,password)
			QQAuth = self.QQ_Auth(username,password)
			RRAuth = self.RR_Auth(username,password)
			if BAuth:
				return BAuth
			if SinaAuth:
				return SinaAuth
			if QQAuth:
				return QQAuth
			if RRAuth:
				return RRAuth
			return False
		except Exception,e:
			"if there is no cookie"
			return False
	def username(self):
		"""docstring for username"""
		return web.cookies().username
	
	def password(self):
		"""docstring for password"""
		return web.cookies().password
	
	def level(self):
		return web.cookies().level
	
	def SET(self,COOKIE_EXPIRE=COOKIE_EXPIRE,**kwargs):
		for key in kwargs.keys():
			web.setcookie(key,encrypt(kwargs[key]),COOKIE_EXPIRE)
	
	def BasicAuth(self,username,password):
		return self.u.BasicAuth(username,password)
	
	def Sina_Auth(self,uid,access_tolken):
		return self.u.SinaAuth(uid,access_tolken)
	
	def QQ_Auth(self,uid,access_tolken):
		return self.u.QQAuth(uid,access_tolken)
	
	def RR_Auth(self,uid,access_tolken):
		return self.u.RRAuth(uid,access_tolken)