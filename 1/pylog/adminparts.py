# -*- coding: utf-8 -*-
import pylog.sphc as sphc
import os
import web
from pylog.common import split
from pylog.common import strfmonth

TEMPLATE_PATH = '../templates/themes/desktop/'
PARTS = '/parts'

TSET = {}
TSET['Setting'] = 'settings'
TSET['Link'] = 'links'
TSET['Blog'] = 'blogs'
TSET['Note'] = 'notes'
TSET['User'] = 'users'
TSET['Tag'] = 'tags'
TSET['Comment'] = 'comments'

Key_TITLE = 'title'
Key_TYPE = 'type'
SNIPPETS = {'split':split,'len':len,'strfmonth':strfmonth}

from pylog.auth import Cookie
from modules.Users import Users
User = Users(TSET['User'])
cookie = Cookie(User)


app_root = os.path.abspath(os.path.join('..',os.path.dirname(__file__)))
from modules.Settings import Settings
Settings = Settings(TSET['Setting'])

THEME = Settings.get(Key_TITLE,'admin_theme')[0].value
templates_root = os.path.join(app_root,TEMPLATE_PATH+THEME+PARTS)
render = web.template.render(templates_root,globals=SNIPPETS)

PAGE_LENGTH = int(Settings.get(Key_TITLE,'PageLength')[0].value)

renderDict = {}
renderKeys = []

class Common():
	
	def __init__(self):
		self.TF = sphc.TagFactory()

	def _message(self,title,message):
		p1 = str(self.TF.h3(title))
		p2 = str(self.TF.p(message))
		return p1 + p2
	
	#data = [(a,b),(b,b)]
	def _table(self,data,node,tk="",trk="",tdk=""):
		t = self.TF.table(Class=tk)
		for element in data:
			row = self.TF.tr(Class=trk)
			for i in element:
				i.reverse()
				id = i[1]
				for item in i:
					row.cells = self.TF.td(self.TF.a(str(item),href="/manage/edit_" + node + "/"+str(id)))
			t.row = row
		return str(t)
	
	def _percentage(self,id,color,value,total):
		percent = int((value/total)*100)
		klass = "progress full progress-%s" % color
		style = "width: %s%; display: block;" % percent
		div = self.TF.div(self.TF.span(self.TF.b(str(percent)+"%",style="display:inline;"),style=style),id=id,Class=klass,value=value)
		return str(div)
	
	def _subtitle(self,words):
		return str(self.TF.span(words,Class="subtitle"))
	
	def _msgstrip(self,type,msg):
		#info warning error error-msg
		klass = "box box-%s"
		return str(self.TF.div(msg,Class=klass))
	
	def _msgbox(self,type,msg):
		msgklass = "box box-%s-msg" % type
		p1 = self._msgstrip(type,"%s MESSAGE" % type)
		p2 = str(self.TF.div(msg,Class=msgklass))
		return p1 + p2
	
	def _label(self,color,name):
		klass = "label label-%s" % color
		return str(self.TF.span(name,Class=klass))
	
	def _button(self,color,name,href):
		klass = "btn btn-%s" % color
		return self.TF.a(name,Class=klass,href=href)
	
	def _button_spec(self,color,name,href):
		klass = "btn btn-special btn-%s" % color
		return self.TF.a(name,Class=klass,href=href)
	
	def _button_icon(self,name,href,icon):
		#ok cancel add
		klass = "icon icon-%s" % icon
		return str(self.TF.a(self.TF.span("&nbsp;",Class=klass),name,Class="btn"))
	
	def _main_navi(self,current):
		from modules.Links import Links
		usr = cookie.GET()
		Links = Links(TSET['Link'])
		myvar = dict(type="admin_nav",weight=usr.level)
		links = Links.sdb.select(Links.table,myvar,where="type=$type AND weight >= 0 AND weight < $weight")
		linkDict = {}
		result = []
		sub_li = "float: left; width: 100%; white-space: normal; "
		sub_link = "float: none; width: auto; "
		sub_link_k = "sf-with-ul"
		lvl2_ul_style = "left: 12em; float: none; width: 12em; display: none; visibility: hidden; "
		lvl1_ul_style = "float: none; width: 12em; display: none; visibility: hidden; "
		ul_klass = "sf-js-enabled sf-shadow"
		ul_menu_klass = "sf-menu sf-js-enabled sf-shadow"
		sub_indicator = self.TF.span(" »",Class="sf-sub-indicator")
		for link in links:
			if not link.parent in linkDict.keys():
				linkDict[link.parent] = [link]
			else:
				linkDict[link.parent].append(link)
		
		for link in linkDict[0]:
			if link.url == current:
				klass = "current"
			else:
				klass = ""
			if not link.id in linkDict.keys():
				result.append(self.TF.li(self.TF.a(link.title,href="/manage/%s/" % link.title),Class=klass))
			else:
				result_level_1 = []
				for link_s in linkDict[link.id]:
					if not link_s.id in linkDict.keys():
						result_level_1.append(self.TF.li(self.TF.a(link_s.title,href="/manage/%s/" % link_s.title,style=sub_link),style=sub_li))
					else:
						result_level_2 = []
						for link_ss in linkDict[link_s.id]:
							result_level_2.append(self.TF.li(self.TF.a(link_ss.title,href="/manage/%s/" % link_ss.title,style=sub_link),style=sub_li))
						
						r_s = self.TF.ul(result_level_2,style=lvl2_ul_style,Class=ul_klass)
						result_level_1.append(str(self.TF.li([self.TF.a([link_s.title,sub_indicator,sub_indicator],href="/manage/%s/" % link_s.title,style=sub_link,Class=sub_link_k),r_s],style=sub_li)))
				r = self.TF.ul(result_level_1,style=lvl1_ul_style,Class=ul_klass)
				result.append(str(self.TF.li([self.TF.a([link.title,sub_indicator],href="/manage/%s/" % link.title,Class=sub_link_k),r],Class=klass)))
		
		return str(self.TF.nav(self.TF.ul(result,Class=ul_menu_klass),id="menu"))
	
	def _content_box(self,title,body):
		T = self.TF.header(self.TF.h3(title),style="cursor: s-resize;")
		B = self.TF.section(body)
		div = self.TF.div([T,B],Class="content-box")
		return div

	def head(self):
		renderDict['user'] = cookie.GET()
		return render.head(renderDict)
	
	def header(self,current):
		renderDict['title'] = Settings.get(Key_TITLE,'title')[0].value
		renderDict['user'] = cookie.GET()
		renderDict['nav'] = self._main_navi(current)
		return render.header(renderDict)
	
	def title(self,current):
		renderDict['title'] = current
		return render.pagetitle(renderDict)
	
	def footer(self):
		from modules.Links import Links
		from datetime import date
		Links = Links(TSET['Link'])
		usr = cookie.GET()
		myvar = dict(type="admin_nav",weight=usr.level)
		renderDict['links'] = Links.sdb.select(Links.table,myvar,where="type=$type AND weight < $weight AND weight >= 0 AND parent=0")
		renderDict['year'] = date.today().year
		return render.footer(renderDict)

	def welcome(self,greetings,username,info):
		p1 = ",".join([greetings,str(self.TF.a(username,href="/"))])
		return self._message(p1,info)

	def right_menu(self,node,link):
		renderDict['node'] = node
		renderDict['link'] = link
		return render.right_menu(renderDict)
	
	def settings(self,settings):
		renderDict['settings'] = settings
		return render.site_settings(renderDict)
	
	def navigation_settings(self,out,admin):
		renderDict['out'] = out
		renderDict['admin'] = admin
		return render.navigation_settings(renderDict)
	
	def myaccount(self,user):
		renderDict['user'] = user
		return render.mypage(renderDict)
	
	def myinfo(self,user):
		renderDict['user'] = user
		if "" in user.values():
			dc = len(set(user.values())) - 1
		else:
			dc = len(set(user.values()))
		renderDict['data_accomplishment'] = dc
		return render.myinfo(renderDict)	

	def dashboard(self,user):
		renderDict['user'] = user 
		return render.dashboard(renderDict)
	
	def node(self,**kwargs):
		for key in kwargs.keys():
			renderDict[key] = kwargs[key]
		return render.node(renderDict)
	
	def node_side(self,**kwargs):
		for key in kwargs.keys():
			renderDict[key] = kwargs[key]
		return render.node_side(renderDict)