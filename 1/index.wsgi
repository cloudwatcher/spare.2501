import os

import sae
import web

from views.index import index
from views.blog import page
from views.blog import article
from views.blog import archive
from views.admin import login
from views.admin import manage
from views.oauth import oauth

web.config.debug = True

urls = (
	'/','index',
	'/Blog/Page/(.*)','page',
	'/Blog/Article/(.*)','article',
	'/Blog/Archive/(.*)','archive',
	'/Admin/Login/','login',
	'/manage/(.*)','manage',
	'/oauth/(.*)','oauth'
)

app = web.application(urls, globals())

application = sae.create_wsgi_app(app.wsgifunc())