#!/usr/bin/env python
# encoding: utf-8
"""
Tags.py

Created by 姚 远 on 2011-12-27.
Copyright (c) 2011 ManaFlame. All rights reserved.
"""
from baseModule import Base

class Tags(Base):
	"""docstring for Settings"""
	def add(self,title,body,author):
		id = self.last_id() + 1
		self.sdb.insert(self.table,title=title,body=body,author=author,id=id)