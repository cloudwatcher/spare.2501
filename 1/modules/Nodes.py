#!/usr/bin/env python
# encoding: utf-8
"""
Nodes.py

Created by 姚 远 on 2011-12-27.
Copyright (c) 2011 ManaFlame. All rights reserved.
"""
from baseModule import Base
import web.db as DB

class Nodes(Base):
	"""docstring for Settings"""
	def add(self,title,body,author,NodeType="normal"):
		id = self.last_id() + 1
		self.sdb.insert(self.table,title=title,body=body,author=author,id=id,type=NodeType)
	def update(self,title,body,author,NodeID,NodeType="normal"):
		where = "id = %s" % NodeID
		self.sdb.update(self.table,where=where,title=title,body=body)
	
	def page(self,PNum,PAGE_LENGTH,TAG=None,AUTHOR=None,COMMENTCOUNT=None,ID=False,DATE=True,BODY=True,WHERE=""):
		A = PAGE_LENGTH * PNum
		B = PAGE_LENGTH * (PNum - 1)

		SQLDICT = {
			'title':'node.title title', 
			'id':'node.id',
			'body':'node.body', 
			'date':'node.rdate', 
			'author':'author.title author',
			'comments':'GROUP_CONCAT(DISTINCT comment.id) comments',
			'tags':'GROUP_CONCAT(DISTINCT tag.title) tags'
		}
		
		JOINDICT = {
			'node':"%s node" % self.table,
			'author':"INNER JOIN %s author ON node.author = author.id" % AUTHOR,
			'comments':"""LEFT JOIN %s comment ON node.id = comment.parent AND comment.type = '%s'""" % (COMMENTCOUNT,self.table),
			'tags':"""LEFT JOIN %s tag ON node.id = tag.rid AND tag.type = '%s'""" % (TAG,self.table)
		}
		
		if not TAG:
			del SQLDICT['tags']
			del JOINDICT['tags']
		if not AUTHOR:
			del SQLDICT['author']
			del JOINDICT['author']
		if not COMMENTCOUNT:
			del SQLDICT['comments']
			del JOINDICT['comments']
		if not DATE:
			del SQLDICT['date']
		if not BODY:
			del SQLDICT['body']
		if not ID:
			del SQLDICT['id']

		ALL_STATEMENT = [
			'SELECT', DB.sqllist(SQLDICT.values()),
			'FROM'," ( " * (len(JOINDICT.values())-1) + " ) ".join(JOINDICT.values()),
			WHERE,"""GROUP BY node.id ORDER BY node.id DESC LIMIT %s,%s""" % (B,A)
			]
		
		SQL = " ".join(ALL_STATEMENT)
		return self.sdb.query(SQL)

	def parent(self,AUTHOR,parent):
		SQL = """
		SELECT node.title, node.body, node.rdate, author.title author , author.avatar avatar FROM
		%s node
		INNER JOIN %s author ON node.author = author.id
		WHERE node.parent = %s
		ORDER BY node.id DESC
		"""
		PARA = (self.table,AUTHOR,parent)
		return self.sdb.query(SQL % PARA)