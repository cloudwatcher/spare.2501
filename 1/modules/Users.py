#!/usr/bin/env python
# encoding: utf-8
"""
Users.py

Created by 姚 远 on 2011-12-27.
Copyright (c) 2011 ManaFlame. All rights reserved.
"""
from baseModule import Base

class Users(Base):
	"""docstring for Settings"""
	def add(self,**kw):
		id = self.last_id() + 1
		self.sdb.insert(self.table,id=id,**kw)
	
	def BasicAuth(self,username,password):
		try:
			ui = self.sdb.where(self.table,mail_addr=username)[0]
		except:
			return 0
		if password == ui.password:
			return ui
		else:
			return 0
	
	def SinaAuth(self,uid,ac):
		try:
			ui = self.sdb.where(self.table,sina_id=uid)[0]
		except:
			return 0
		if ac == ui.sina_access_tolken:
			return ui
		else:
			return 0
	
	def QQAuth(self,uid,ac):
		try:
			ui = self.sdb.where(self.table,qq_id=uid)[0]
		except:
			return 0
		if ac == ui.qq_access_tolken:
			return ui
		else:
			return 0

	def RRAuth(self,uid,ac):
		try:
			ui = self.sdb.where(self.table,rr_id=uid)[0]
		except:
			return 0
		if ac == ui.rr_access_tolken:
			return ui
		else:
			return 0